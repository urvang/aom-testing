# Copyright (c) 2019, Alliance for Open Media. All rights reserved
#
# This source code is subject to the terms of the BSD 2 Clause License and
# the Alliance for Open Media Patent License 1.0. If the BSD 2 Clause License
# was not distributed with this source code in the LICENSE file, you can
# obtain it at www.aomedia.org/license/software. If the Alliance for Open
# Media Patent License 1.0 was not distributed with this source code in the
# PATENTS file, you can obtain it at www.aomedia.org/license/patent.

from __future__ import print_function  # Only needed for Python 2
import os
import string
import re
import sys
import operator
import subprocess
import glob
import binascii

'''Encoder Template'''
encoder                       = "svt"
#encoder                     = "vvenc"
# encoder                     = "aom"
# encoder                     = "x264"
# encoder                     = "x265"
# encoder                     = "vvenc"
# encoder                     = "ffmpeg"
# encoder                     = "vp9"

metrics_computation = 2    ### [0/1]: 0 = libvmaf, 1 = ffmpeg, 2= Vmaf
shift_resolution=0         ### automation compatibility
offline_bitrate_calculation=0

'''This flag indicates collecting the bitstream size **INSTEAD** of bitrate'''
filesize_instead            = 0

'''This Flag signifies that both bitrate and file_size will be added to the results file, the main metric being decided by the filesize_instead'''
keep_both_bitrate_filesize_column = 0

'''one log file extension present in folder'''
f_ext                       = ".log"

'''only files with prefix will be parsed'''
prefix                      = ""
log_folder_path             = "bitstreams"

'''print output to log'''
output_log                  = 1
log_name                    = "_results.txt"

'''mute console output'''
'''this will be forced ON if output_log == 0 ||  log_name == ""'''
silent_console              = 1

'''Get enc name displayed is based on the parent folder name (..) [if this option is disabled, the file name would be used to extract the enc_name]'''
get_enc_name_from_folder    = 0

'''Force enc name to be the value of enc_name [this option supersedes both options above] -> This now parses delimiter at the end from file name'''
force_enc_name              = 1
enc_name                    = "svt"
log_name='%s_%s.txt'%(log_name.replace('.txt',''),enc_name)

'''This is the 8/1/1 PSNR SSIM script'''
y_scale                     = 8     ## 8/1/1

'''This is for getting the true encode/decode resolution based on name'''
# downscale_upscale_bdrate    = 0 #Auto turn on for downscale pipeline

##'''This flag indicates whether or not the vmaf_rc executable was used'''
##vmaf_rc_flag                = 0


'''Convex Hull Calculation'''
compute_convex_hull        = 0

'''output file for convex hull points'''
output_file_suffix         = "_convex_hull_data.txt"

'''add lines until this number is reached'''
max_ch_pts  = 40

'''metrics to compute the convex hull on'''
'''choose from a mix of psnr, vmaf, and ssim'''
if metrics_computation == 0:
    metrics_computation == 2

if metrics_computation == 1:
    cvh_metrics = ["psnr","vmaf", "ssim"]
elif metrics_computation == 2:
    cvh_metrics = ["psnr","ssim",'vmaf','vmaf-neg']

else:
    cvh_metrics = ["psnr","vmaf", "ssim", "vmaf_neg"]




T1_SET = ['aspen_720p_60f', 'BasketballDrillText_832x480_50_60f', 'BasketballDrill_832x480_50_60f',
          'BasketballPass_416x240_50_60f', 'BlowingBubbles_416x240_50_60f', 'blue_sky_360p_60f',
          'BQMall_832x480_60_60f', 'BQSquare_416x240_60_60f', 'CatRobot_1280x720_60fps_8bit_420_jvet_60f',
          'ChinaSpeed_1024x768_30_60f', 'DOTA2_60f_420_720p', 'FoodMarket4_1280x720_60fps_8bit_420_60f',
          'FourPeople_1280x720_60_60f', 'gipsrestat720p_60f', 'Johnny_1280x720_60_60f',
          'kirland360p_60f', 'KristenAndSara_1280x720_60f', 'Netflix_BarScene_1280x720_60fps_true_8bit_420_60f',
          'Netflix_BoxingPractice_1280x720_60fps_true_8bit_420_60f', 'Netflix_Crosswalk_1280x720_60fps_true_8bit_420_60f',
          'Netflix_Dancers_1280x720_60fps_true_8bit_420_60f', 'Netflix_Narrator_1280x720_60fps_true_8bit_420_60f',
          'Netflix_PierSeaside_1280x720_60fps_true_8bit_420_60f', 'Netflix_RitualDance_1280x720_60fps_true_8bit_420_60f',
          'Netflix_WindAndNature_1280x720_60fps_true_8bit_420_60f', 'niklas360p_60f', 'PartyScene_832x480_50_60f',
          'RaceHorses_416x240_30_60f', 'RaceHorses_832x480_30_60f', 'red_kayak_360p_60f', 'shields_640x360_60f',
          'SlideEditing_1280x720_30_60f', 'SlideShow_1280x720_20_60f', 'speed_bag_640x360_60f',
          'STARCRAFT_60f_420_720p', 'thaloundeskmtg360p_60f', 'touchdown_pass_720p_60f',
          'vidyo1_720p_60fps_60f', 'vidyo4_720p_60fps_60f', 'wikipedia_420_720p']

T2_SET = ['BasketballDrive_1280x720_50_60f', 'BQTerrace_1280x720_60_60f', 'Cactus_1280x720_50_60f',
          'Campfire_1280x720_30fps_8bit_bt709_420_videoRange_60f', 'dark720p_60f', 'DaylightRoad2_1280x720_60fps_8bit_420_60f',
          'ducks_take_off_720p50_60f', 'Kimono1_1280x720_24_60f', 'life_720p30_60f', 'MarketPlace_1280x720_60fps_8bit_420_60f',
          'MINECRAFT_60f_420_720p', 'Netflix_Aerial_1280x720_60fps_true_8bit_420_60f', 'Netflix_Boat_1280x720_60fps_true_8bit_420_60f',
          'Netflix_DrivingPOV_1280x720_60fps_8bit_420_60f', 'Netflix_FoodMarket_1280x720_60fps_true_8bit_420_60f',
          'Netflix_RollerCoaster_1280x720_60fps_8bit_420_60f', 'Netflix_SquareAndTimelapse_1280x720_60fps_true_8bit_420_60f',
          'Netflix_ToddlerFountain_1280x720_60fps_true_8bit_420_60f', 'Netflix_TunnelFlag_1280x720_60fps_true_8bit_420_60f',
          'ParkRunning3_1280x720_50fps_8bit_420_60f', 'ParkScene_1280x720_24_60f', 'PeopleOnStreet_1280x720_30_crop_60f',
          'rush_hour_720p25_60f', 'Tango2_1280x720_60fps_8bit_420_60f', 'Traffic_1280x720_30_crop_60f']

AOM_testset = ['aspen_1080p_60f', 'DOTA2_60f_420', 'ducks_take_off_1080p50_60f', 'life_1080p30_60f', 'Netflix_Aerial_1920x1080_60fps_8bit_420_60f',
               'Netflix_Boat_1920x1080_60fps_8bit_420_60f', 'Netflix_Crosswalk_1920x1080_60fps_8bit_420_60f', 'Netflix_FoodMarket_1920x1080_60fps_8bit_420_60f',
               'Netflix_PierSeaside_1920x1080_60fps_8bit_420_60f', 'Netflix_SquareAndTimelapse_1920x1080_60fps_8bit_420_60f', 'Netflix_TunnelFlag_1920x1080_60fps_8bit_420_60f',
               'rush_hour_1080p25_60f', 'STARCRAFT_60f_420', 'touchdown_pass_1080p_60f', 'MINECRAFT_60f_420', 'dark720p_60f', 'gipsrestat720p_60f',
               'KristenAndSara_1280x720_60f', 'Netflix_DrivingPOV_1280x720_60fps_8bit_420_60f', 'Netflix_RollerCoaster_1280x720_60fps_8bit_420_60f', 'vidyo1_720p_60fps_60f',
               'vidyo4_720p_60fps_60f', 'blue_sky_360p_60f', 'kirland360p_60f', 'niklas360p_60f', 'red_kayak_360p_60f', 'shields_640x360_60f', 'speed_bag_640x360_60f',
               'thaloundeskmtg360p_60f', 'Netflix_Aerial_1280x720_60fps_true_10bit_420_60f', 'Netflix_BarScene_1280x720_60fps_true_10bit_420_60f', 'Netflix_Boat_1280x720_60fps_true_10bit_420_60f',
               'Netflix_BoxingPractice_1280x720_60fps_true_10bit_420_60f', 'Netflix_Crosswalk_1280x720_60fps_true_10bit_420_60f', 'Netflix_Dancers_1280x720_60fps_true_10bit_420_60f',
               'Netflix_FoodMarket_1280x720_60fps_true_10bit_420_60f', 'Netflix_Narrator_1280x720_60fps_true_10bit_420_60f', 'Netflix_PierSeaside_1280x720_60fps_true_10bit_420_60f',
               'Netflix_RitualDance_1280x720_60fps_true_10bit_420_60f', 'Netflix_SquareAndTimelapse_1280x720_60fps_true_10bit_420_60f', 'Netflix_ToddlerFountain_1280x720_60fps_true_10bit_420_60f',
               'Netflix_TunnelFlag_1280x720_60fps_true_10bit_420_60f', 'Netflix_WindAndNature_1280x720_60fps_true_10bit_420_60f', 'Netflix_Aerial_1280x720_60fps_true_8bit_420_60f',
               'Netflix_BarScene_1280x720_60fps_true_8bit_420_60f', 'Netflix_Boat_1280x720_60fps_true_8bit_420_60f', 'Netflix_BoxingPractice_1280x720_60fps_true_8bit_420_60f',
               'Netflix_Crosswalk_1280x720_60fps_true_8bit_420_60f', 'Netflix_Dancers_1280x720_60fps_true_8bit_420_60f', 'Netflix_FoodMarket_1280x720_60fps_true_8bit_420_60f',
               'Netflix_Narrator_1280x720_60fps_true_8bit_420_60f', 'Netflix_PierSeaside_1280x720_60fps_true_8bit_420_60f', 'Netflix_RitualDance_1280x720_60fps_true_8bit_420_60f',
               'Netflix_SquareAndTimelapse_1280x720_60fps_true_8bit_420_60f', 'Netflix_ToddlerFountain_1280x720_60fps_true_8bit_420_60f', 'Netflix_TunnelFlag_1280x720_60fps_true_8bit_420_60f',
               'Netflix_WindAndNature_1280x720_60fps_true_8bit_420_60f', 'aspen_1080p_60f_10bit', 'DOTA2_60f_420_10bit', 'ducks_take_off_1080p50_60f_10bit', 'life_1080p30_60f_10bit',
               'Netflix_Aerial_1920x1080_60fps_8bit_420_60f_10bit', 'Netflix_Boat_1920x1080_60fps_8bit_420_60f_10bit', 'Netflix_Crosswalk_1920x1080_60fps_8bit_420_60f_10bit',
               'Netflix_FoodMarket_1920x1080_60fps_8bit_420_60f_10bit', 'Netflix_PierSeaside_1920x1080_60fps_8bit_420_60f_10bit', 'Netflix_SquareAndTimelapse_1920x1080_60fps_8bit_420_60f_10bit',
               'Netflix_TunnelFlag_1920x1080_60fps_8bit_420_60f_10bit', 'rush_hour_1080p25_60f_10bit', 'STARCRAFT_60f_420_10bit', 'touchdown_pass_1080p_60f_10bit',
               'MINECRAFT_60f_420_10bit', 'dark720p_60f_10bit', 'gipsrestat720p_60f_10bit', 'KristenAndSara_1280x720_60f_10bit', 'wikipedia_420_10bit',
               'Netflix_DrivingPOV_1280x720_60fps_8bit_420_60f_10bit', 'Netflix_RollerCoaster_1280x720_60fps_8bit_420_60f_10bit', 'vidyo1_720p_60fps_60f_10bit',
               'vidyo4_720p_60fps_60f_10bit', 'blue_sky_360p_60f_10bit', 'kirland360p_60f_10bit', 'niklas360p_60f_10bit', 'red_kayak_360p_60f_10bit', 'shields_640x360_60f_10bit',
               'speed_bag_640x360_60f_10bit', 'thaloundeskmtg360p_60f_10bit', 'wikipedia_420']

jctvc_60frames_TestSet = ['BasketballDrill_832x480_50_60f', 'BasketballDrillText_832x480_50_60f', 'BasketballDrive_1920x1080_50_60f', 'BasketballPass_416x240_50_60f',
                          'BlowingBubbles_416x240_50_60f', 'BQMall_832x480_60_60f', 'BQSquare_416x240_60_60f', 'BQTerrace_1920x1080_60_60f', 'Cactus_1920x1080_50_60f',
                          'ChinaSpeed_1024x768_30_60f', 'FourPeople_1280x720_60_60f', 'Johnny_1280x720_60_60f', 'Kimono1_1920x1080_24_60f', 'KristenAndSara_1280x720_60_60f',
                          'ParkScene_1920x1080_24_60f', 'PartyScene_832x480_50_60f', 'PeopleOnStreet_2560x1600_30_crop_60f', 'RaceHorses_416x240_30_60f', 'RaceHorses_832x480_30_60f',
                          'SlideEditing_1280x720_30_60f', 'SlideShow_1280x720_20_60f', 'Traffic_2560x1600_30_crop_60f', 'Campfire_3840x2160_30fps_10bit_bt709_420_videoRange_60f',
                          'DaylightRoad2_3840x2160_60fps_10bit_420_60f', 'FoodMarket4_3840x2160_60fps_10bit_420_60f', 'Tango2_3840x2160_60fps_10bit_420_60f',
                          'ParkRunning3_3840x2160_50fps_10bit_420_60f', 'CatRobot_3840x2160_60fps_10bit_420_jvet_60f', 'RitualDance_1920x1080_60fps_10bit_420_60f',
                          'MarketPlace_1920x1080_60fps_10bit_420_60f']

jctv_fullframes_Testset = ['BasketballDrill_832x480_50', 'BasketballDrillText_832x480_50', 'BasketballDrive_1920x1080_50', 'BasketballPass_416x240_50',
                           'BlowingBubbles_416x240_50', 'BQMall_832x480_60', 'BQSquare_416x240_60', 'BQTerrace_1920x1080_60', 'Cactus_1920x1080_50',
                           'ChinaSpeed_1024x768_30', 'FourPeople_1280x720_60', 'Johnny_1280x720_60', 'Kimono1_1920x1080_24', 'KristenAndSara_1280x720_60',
                           'ParkScene_1920x1080_24', 'PartyScene_832x480_50', 'PeopleOnStreet_2560x1600_30_crop', 'RaceHorses_416x240_30', 'RaceHorses_832x480_30',
                           'SlideEditing_1280x720_30', 'SlideShow_1280x720_20', 'Traffic_2560x1600_30_crop', 'Campfire_3840x2160_30fps_10bit_bt709_420_videoRange',
                           'DaylightRoad2_3840x2160_60fps_10bit_420', 'FoodMarket4_3840x2160_60fps_10bit_420', 'Tango2_3840x2160_60fps_10bit_420',
                           'ParkRunning3_3840x2160_50fps_10bit_420', 'CatRobot_3840x2160_60fps_10bit_420_jvet', 'RitualDance_1920x1080_60fps_10bit_420',
                           'MarketPlace_1920x1080_60fps_10bit_420']

Testset_4k_14 = [ 'Netflix_TunnelFlag_4096x2160_60fps_10bit_420_60f', 'Netflix_PierSeaside_4096x2160_60fps_10bit_420_60f',
                  'Netflix_FoodMarket_4096x2160_60fps_10bit_420_60f','Netflix_Boat_4096x2160_60fps_10bit_420_60f',
                  'Netflix_SquareAndTimelapse_4096x2160_60fps_10bit_420_60f','Netflix_Crosswalk_4096x2160_60fps_10bit_420_60f',
                  'Netflix_Aerial_4096x2160_60fps_10bit_420_60f','Netflix_BoxingPractice_4096x2160_60fps_10bit_420_60f',
                  'Netflix_WindAndNature_4096x2160_60fps_10bit_420_60f','Netflix_ToddlerFountain_4096x2160_60fps_10bit_420_60f',
                  'Netflix_RitualDance_4096x2160_60fps_10bit_420_60f','Netflix_Narrator_4096x2160_60fps_10bit_420_60f',
                  'Netflix_Dancers_4096x2160_60fps_10bit_420_60f','Netflix_BarScene_4096x2160_60fps_10bit_420_60f']

TestSet_49f = ['aspen_1080p_49f','blue_sky_360p_49f','dark720p_49f','DOTA2_49f_420',
               'ducks_take_off_1080p50_49f','gipsrestat360p_49f','life_1080p30_49f','kirland360p_49f',
               'KristenAndSara_1280x720_49f','MINECRAFT_49f_420','Netflix_Aerial_1920x1080_60fps_8bit_420_49f',
               'Netflix_Boat_1920x1080_60fps_8bit_420_49f','Netflix_Crosswalk_1920x1080_60fps_8bit_420_49f',
               'Netflix_DrivingPOV_1280x720_60fps_8bit_420_49f','Netflix_FoodMarket_1920x1080_60fps_8bit_420_49f',
               'Netflix_PierSeaside_1920x1080_60fps_8bit_420_49f','Netflix_RollerCoaster_1280x720_60fps_8bit_420_49f',
               'Netflix_SquareAndTimelapse_1920x1080_60fps_8bit_420_49f','Netflix_TunnelFlag_1920x1080_60fps_8bit_420_49f',
               'niklas360p_49f','red_kayak_360p_49f','rush_hour_1080p25_49f','shields_640x360_49f',
               'speed_bag_640x360_49f','STARCRAFT_49f_420','thaloundeskmtg360p_49f','touchdown_pass_1080p_49f',
               'vidyo1_360p_60fps_49f','vidyo4_360p_60fps_49f','wikipedia_420_49f']

def print_log(out_to_log,mode, index_i):

    if output_log == 1 and log_name != "":
        try:
            print (out_to_log,file=open(log_name,mode))
        except:
            if silent_console == 0:
                print (out_to_log)
    elif silent_console == 0:
        print (out_to_log)

def read_y4m_header(clip_path):

    if sys.version_info[0] ==3:
        header_delimiters = [b"W",b"H",b"F",b"I",b"A",b"C"]
    else:
        header_delimiters = ["W","H","F","I","A","C"]
    width = 0
    height = 0
    frame_ratio = ""
    framerate = 0
    number_of_frames=1
    import binascii
    #print(clip_path)
    with open(clip_path, "rb") as f:
        f.seek(10)
        if sys.version_info[0] ==3: buffer = b""
        else:   buffer=""
        while True:
            readByte = f.read(1)

            if (readByte in header_delimiters):
                if readByte == header_delimiters[0]:
                    while 1:
                        readByte = f.read(1)
                        if sys.version_info[0] ==3:
                            if (readByte == b'\n' or readByte==b' '):
                                width = int(buffer)
                                buffer = b""
                                break
                            else:
                                buffer+=readByte
                        else:
                            if (readByte == '\n' or readByte==' '):
                                width = int(buffer)
                                buffer = ""
                                break
                            else:
                                buffer+=readByte
                elif readByte == header_delimiters[1]:
                    while 1:
                        readByte = f.read(1)
                        if sys.version_info[0] ==3:
                            if (readByte == b'\n' or readByte==b' '):
                                height = int(buffer)
                                buffer = b""
                                break
                            else:
                                buffer+=readByte
                        else:
                            if (readByte == '\n' or readByte==' '):
                                height = int(buffer)
                                buffer = ""
                                break
                            else:
                                buffer+=readByte
                elif readByte == header_delimiters[2]:
                    while 1:
                        readByte = f.read(1)
                        if sys.version_info[0] ==3:
                            if (readByte == b'\n' or readByte==b' '):
                                frame_ratio = buffer
                                buffer = b""
                                break
                            else:
                                buffer+=readByte
                        else:
                            if (readByte == '\n' or readByte==' '):
                                frame_ratio = buffer
                                buffer = ""
                                break
                            else:
                                buffer+=readByte
                elif readByte == header_delimiters[5]:
                    while 1:
                        readByte = f.read(1)

                        if sys.version_info[0] ==3:
                            if (readByte == b'\n' or readByte==b' '):
                                if b'=' in buffer:
                                    break
                                if b'10' in buffer:
                                    bit_depth='10bit'
                                else:
                                    bit_depth='8bit'
                                #bit_depth = buffer
                                buffer = b""
                                break
                            else:
                                buffer+=readByte
                        else:

                            if (readByte == '\n' or readByte==' '):
                                if '=' in buffer:
                                    break
                                if '10' in buffer:
                                    bit_depth='10bit'
                                else:
                                    bit_depth='8bit'
                                #bit_depth = buffer
                                buffer = ""
                                break
                            else:
                                buffer+=readByte
            if sys.version_info[0] ==3:
                if binascii.hexlify(readByte) == b'0a':
                    break
            else:
                if binascii.hexlify(readByte) == '0a':
                    break
        # other_counter=0
        # print('width',width)
        # print('height',height)
        # print('bit_depth',bit_depth)
        # print('os.path.getsize(clip_path)/60',os.path.getsize(clip_path)/60)
        # print('os.path.getsize(clip_path))',os.path.getsize(clip_path))
        if bit_depth=='10bit':
            frame_length = int(float(2)*float(width * height * float(3) / 2))
        else:
            frame_length = int(width * height * float(3) / 2)
        # print('frame_length',frame_length)
        #frame_length=6220807
        # print(os.path.getsize(clip_path))
        while f.tell() < os.path.getsize(clip_path):
            readByte = f.read(1)
            if binascii.hexlify(readByte) == b'0a':
                # other_counter+=1
                f.seek(frame_length,1)
                buff=binascii.hexlify(f.read(5))
                #print('buff',buff)
                if buff == b'4652414d45':
                    number_of_frames+=1
        # print('other_counter',other_counter)
        #print('number_of_frames',number_of_frames)
        frame_ratio_pieces = frame_ratio.split(":")
        framerate = float(frame_ratio_pieces[0])/float(frame_ratio_pieces[1])
##        print('width',width)
##        print('height',height)
##        print('framerate',framerate)

    return width, height, framerate, number_of_frames


if metrics_computation == 1:
    out_header =   ("Codec"
                 + "\t" + "EncoderName"
                 + "\t" + "Resolution"
                 + "\t" + "Bit-Depth"
                 + "\t" + "InputSequence"
                 + "\t" + "QP"
                 + "\t" + "Kbps"
                 + "\t" + "PSNR(Y)"
                 + "\t" + "PSNR(U)"
                 + "\t" + "PSNR(V)"
                 + "\t" + "PSNR(ALL)"
                 + "\t" + "SSIM(Y)"
                 + "\t" + "SSIM(U)"
                 + "\t" + "SSIM(V)"
                 + "\t" + "SSIM(ALL)"
                 + "\t" + "VMAF"
                 + "\t" + "enc_time(ms)"
                 + "\t" + "wall_time(ms)"
                 + "\t" + "max_memory(kb)"
                 + "\t" + "sys_time(ms)")
    if filesize_instead == 1:
        out_header =   ("Codec"
                     + "\t" + "EncoderName"
                     + "\t" + "Resolution"
                     + "\t" + "Bit-Depth"
                     + "\t" + "InputSequence"
                     + "\t" + "QP"
                     + "\t" + "FileSize (bytes)"
                     + "\t" + "PSNR(Y)"
                     + "\t" + "PSNR(U)"
                     + "\t" + "PSNR(V)"
                     + "\t" + "PSNR(ALL)"
                     + "\t" + "SSIM(Y)"
                     + "\t" + "SSIM(U)"
                     + "\t" + "SSIM(V)"
                     + "\t" + "SSIM(ALL)"
                     + "\t" + "VMAF"
                     + "\t" + "enc_time(ms)"
                     + "\t" + "wall_time(ms)"
                     + "\t" + "max_memory(kb)"
                     + "\t" + "sys_time(ms)")
    if keep_both_bitrate_filesize_column and filesize_instead:
        out_header =   ("Codec"
                     + "\t" + "EncoderName"
                     + "\t" + "Resolution"
                     + "\t" + "Bit-Depth"
                     + "\t" + "InputSequence"
                     + "\t" + "QP"
                     + "\t" + "FileSize (bytes)"
                     + "\t" + "PSNR(Y)"
                     + "\t" + "PSNR(U)"
                     + "\t" + "PSNR(V)"
                     + "\t" + "PSNR(ALL)"
                     + "\t" + "SSIM(Y)"
                     + "\t" + "SSIM(U)"
                     + "\t" + "SSIM(V)"
                     + "\t" + "SSIM(ALL)"
                     + "\t" + "VMAF"
                     + "\t" + "enc_time(ms)"
                     + "\t" + "wall_time(ms)"
                     + "\t" + "max_memory(kb)"
                     + "\t" + "sys_time(ms)"
                     + "\t" + "Kbps")

elif metrics_computation == 2: # vmaf
    out_header =   ("Codec"
                 + "\t" + "EncoderName"
                 + "\t" + "Resolution"
                 + "\t" + "Bit-Depth"
                 + "\t" + "InputSequence"
                 + "\t" + "QP"
                 + "\t" + "Kbps"
                 + "\t" + "PSNR(Y)"
                 + "\t" + "Float_SSIM"
                 + "\t" + "VMAF"
                 + "\t" + "VMAF NEG"
                 + "\t" + "enc_time(ms)"
                 + "\t" + "wall_time(ms)"
                 + "\t" + "max_memory(kb)"
                 + "\t" + "sys_time(ms)")
                 # + "\t" + "Kbps")file_size(bytes)
    if filesize_instead == 1:
         out_header =   ("Codec"
                     + "\t" + "EncoderName"
                     + "\t" + "Resolution"
                     + "\t" + "Bit-Depth"
                     + "\t" + "InputSequence"
                     + "\t" + "QP"
                     + "\t" + "FileSize (bytes)"
                     + "\t" + "PSNR(Y)"
                     + "\t" + "Float_SSIM"
                     + "\t" + "VMAF"
                     + "\t" + "VMAF NEG"
                     + "\t" + "enc_time(ms)"
                     + "\t" + "wall_time(ms)"
                     + "\t" + "max_memory(kb)"
                     + "\t" + "sys_time(ms)")
    if keep_both_bitrate_filesize_column and filesize_instead:
         out_header =   ("Codec"
                     + "\t" + "EncoderName"
                     + "\t" + "Resolution"
                     + "\t" + "Bit-Depth"
                     + "\t" + "InputSequence"
                     + "\t" + "QP"
                     + "\t" + "FileSize (bytes)"
                     + "\t" + "PSNR(Y)"
                     + "\t" + "Float_SSIM"
                     + "\t" + "VMAF"
                     + "\t" + "VMAF NEG"
                     + "\t" + "enc_time(ms)"
                     + "\t" + "wall_time(ms)"
                     + "\t" + "max_memory(kb)"
                     + "\t" + "sys_time(ms)"
                     + "\t" + "Kbps")


else:
    out_header =   ("Codec"
                     + "\t" + "EncoderName"
                     + "\t" + "Resolution"
                     + "\t" + "Bit-Depth"
                     + "\t" + "InputSequence"
                     + "\t" + "QP"
                     + "\t" + "Kbps"
                     + "\t" + "PSNR(Y)"
                     + "\t" + "PSNR(U)"
                     + "\t" + "PSNR(V)"
                     + "\t" + "PSNR(ALL)"
                     + "\t" + "SSIM(Y)"
                     + "\t" + "SSIM(U)"
                     + "\t" + "SSIM(V)"
                     + "\t" + "SSIM(ALL)"
                     + "\t" + "VMAF"
                     + "\t" + "VMAF neg"
                     + "\t" + "enc_time(ms)"
                     + "\t" + "wall_time(ms)"
                     + "\t" + "max_memory(kb)"
                     + "\t" + "sys_time(ms)")
    if filesize_instead == 1:
         out_header =   ("Codec"
                     + "\t" + "EncoderName"
                     + "\t" + "Resolution"
                     + "\t" + "Bit-Depth"
                     + "\t" + "InputSequence"
                     + "\t" + "QP"
                     + "\t" + "FileSize (bytes)"
                     + "\t" + "PSNR(Y)"
                     + "\t" + "PSNR(U)"
                     + "\t" + "PSNR(V)"
                     + "\t" + "PSNR(ALL)"
                     + "\t" + "SSIM(Y)"
                     + "\t" + "SSIM(U)"
                     + "\t" + "SSIM(V)"
                     + "\t" + "SSIM(ALL)"
                     + "\t" + "VMAF"
                     + "\t" + "VMAF neg"
                     + "\t" + "enc_time(ms)"
                     + "\t" + "wall_time(ms)"
                     + "\t" + "max_memory(kb)"
                     + "\t" + "sys_time(ms)")
    if keep_both_bitrate_filesize_column and filesize_instead:
         out_header =   ("Codec"
                     + "\t" + "EncoderName"
                     + "\t" + "Resolution"
                     + "\t" + "Bit-Depth"
                     + "\t" + "InputSequence"
                     + "\t" + "QP"
                     + "\t" + "FileSize (bytes)"
                     + "\t" + "PSNR(Y)"
                     + "\t" + "PSNR(U)"
                     + "\t" + "PSNR(V)"
                     + "\t" + "PSNR(ALL)"
                     + "\t" + "SSIM(Y)"
                     + "\t" + "SSIM(U)"
                     + "\t" + "SSIM(V)"
                     + "\t" + "SSIM(ALL)"
                     + "\t" + "VMAF"
                     + "\t" + "VMAF neg"
                     + "\t" + "enc_time(ms)"
                     + "\t" + "wall_time(ms)"
                     + "\t" + "max_memory(kb)"
                     + "\t" + "sys_time(ms)"
                     + "\t" + "Kbps")
if encoder == 'x264' or encoder == 'x265':
    encoder_mode = -1
else:
    encoder_mode = 99

files_dir_list = []
full_data_for_writing = []
bitstreams_files = os.listdir(log_folder_path)
bitstreams_files.sort()
bitstreams_files = [ os.path.join(log_folder_path, i) for i in bitstreams_files]
files_dir_list.append(bitstreams_files)
out_log=open(log_name,'w')
for index_i in range(len(files_dir_list)):
    files = files_dir_list[index_i]
    out_log.write(out_header)
    out_log.write('\n')
    #print_log(out_header,'w', index_i);
    for file_ in files:

        if f_ext in file_ and prefix in file_:
            if '_vmaf.log' in file_:
                file_=file_.replace('_vmaf.log','.log')
            file_name_with_ext = os.path.basename(file_)
            dir_name = os.path.dirname(file_)
            file_name = os.path.splitext(file_name_with_ext)[0]

            if metrics_computation == 1:
            #  read psnr ssim output

                try:
                    if ".log" in file_:
                        psnr_ssim_log = open(os.path.join(dir_name, file_name_with_ext),"r")
                    else:
                        psnr_ssim_log = open(file_name + ".log","r")
                    #print(psnr_ssim_log)
                    lines = psnr_ssim_log.readlines()
                    ssim_line = ""
                    psnr_line = ""
                    res_line = ""
                    for line in lines:
                        if "Parsed_ssim" in line:
                            ssim_line = line
                        if "Parsed_psnr" in line:
                            psnr_line = line
                        if "yuv420p" in line and 'fps' in line or 'tbn' in line:
                            res_line = line
                        if "yuv420p" in line and 'fps' in line:
                            fps_line = line
                        if 'vvdecapp [info]: SizeInfo:' in line:
                            res_line = line
                except:
                    ssim_line = ""
                    psnr_line = ""
                    res_line = ""
                    fps_line = ""


                try:
                    with open(os.path.join(dir_name, file_name+'.vmaf'),"r") as vmaf_log:
                        lines = vmaf_log.readlines()
                        vmaf_line = ""

                        for line in lines:
                            if 'metric name=\"vmaf\"' in line:
                                vmaf_line = line

                except:
                    vmaf_line = ""
                    pass



            else:
            #  read psnr ssim output
                try:
                    if ".log" in file_:
                        psnr_ssim_log = open(os.path.join(dir_name, file_name_with_ext.replace('.log','.log')),"r")
                    else:
                        psnr_ssim_log = open(file_name + ".log","r")
                    lines = psnr_ssim_log.readlines()
                    #print(lines)
                    res_line = ""
                    for i,line in enumerate(lines):
                        # if "yuv420p" and "Stream #1:0: Video: rawvideo" in line:###################
                            # res_line = line

                        # if "yuv420p" in line:#and "Stream #0:0: Video: wrapped_avframe" in line or ' Stream #0:0: Video: rawvideo' in line:
                        if "yuv420p" in line and 'fps' in line:#and "Stream #0:0: Video: wrapped_avframe" in line or ' Stream #0:0: Video: rawvideo' in line:
                            fps_line = line
                            res_line = line
                        if 'vvdecapp [info]: SizeInfo:' in line:
                            res_line = line
                            # print('res_line',res_line)

                except:
                    res_line = ""
                    fps_line = ''

            #  read vmaf output
                try:
                    with open(os.path.join(dir_name, file_name+'.vmaf'),"r") as vmaf_log:
                        lines = vmaf_log.readlines()
                        vmaf_line = ""
                        ssim_line = ""
                        psnr_line = ""
                        for line in lines:
                            if 'metric name=\"vmaf\"' in line:
                                vmaf_line = line
                            if 'metric name=\"psnr\"' in line:
                                psnr_line = line
                            if 'metric name=\"ssim\"' in line:
                                ssim_line = line

                    with open(os.path.join(dir_name, file_name+'.neg_vmaf'),"r") as vmaf_log:
                        lines = vmaf_log.readlines()
                        vmaf_neg_line = ""
                        for line in lines:
                            if 'metric name=\"vmaf\"' in line:
                                vmaf_neg_line = line
                except:
                    vmaf_neg_line = ""
                    vmaf_line = ""
                    ssim_line = ""
                    psnr_line = ""
                    pass

            try:
                bitstream_file_name = os.path.join(dir_name, file_name + '.bin')
                stat_struct = os.stat(bitstream_file_name)
                bitstreams_size = stat_struct.st_size
                # print(bitstream_file_name + " with size " + str(bitstreams_size))
            except:
                bitstreams_size = "err_filesize"
                pass


        #  read bitrate / speed output
            try:
                encode_log = open(os.path.join(dir_name, file_name + ".txt"),"r")
                lines = encode_log.readlines()
                bitrate_line = ""
                enc_time_line = ""
                sys_time_line = ""
                svt_1st_pass_time_line =""
                frames_per_second=''
                fps_line=''
                memory_line = ""
                Num_passes = 0
                for index,raw_line in enumerate(lines):
                    line = str(raw_line).lower()
                    if "vvenc" in encoder:
                        if "bitrate" in line:
                            bitrate_line = line
                        if "user" in line:
                            enc_time_line = line

                    if "bps" in line and "aom" not in encoder and "command being timed" not in line:
                        bitrate_line = line
                    elif "b/s" in line :
                        bitrate_line = line
                    if "ffmpeg" in encoder and "bitrate=" in line:
                        bitrate_line = line
                    if "user" in line and "svt" in encoder:
                        enc_time_line = line
                    if "user" in line and "x264" in encoder:
                        enc_time_line = line
                    if "user" in line and "x265" in encoder:
                        enc_time_line = line
                    if "user" in line and "aom" in encoder:
                        enc_time_line = line
                    if "user" in line and "ffmpeg" in encoder:
                        enc_time_line = line
                    if "system" in line and "time" in line:
                        sys_time_line = line
                    if "maximum" in line :
                        memory_line = line
                    if "encoder finished" in line:
                        Num_passes = Num_passes + 1
                    if 'vp9' in encoder and 'Command being timed:' in line:
                        bitrate_line=lines[index-1]
                    if 'vp9' in encoder and 'user' in line:

                        enc_time_line=line

                    if fps_line == '' and encoder == 'vvenc':
                        if 'framerate' in line:
                            frames_per_second = float(line.split('framerate')[-1].split('-')[0].strip())

                if Num_passes == 2 and "svt" in encoder:
                    for raw_line in lines:
                        line = str(raw_line).lower()
                        if "user time" in line:
                            svt_1st_pass_time_line = line
                            break
            except:
                bitrate_line = ""
                enc_time_line = ""
                sys_time_line = ""
                memory_line = ""
                pass

            #  get output data
            if metrics_computation == 1:
                #  ssim data
                if ssim_line != "":
                    ssim_pieces = ssim_line.split()
                    y_ssim = ssim_pieces[5][1:-1]
                    u_ssim = ssim_pieces[7][1:-1]
                    v_ssim = ssim_pieces[9][1:-1]
                    yuv_ssim = (y_scale * float(y_ssim) + float(u_ssim) + float(v_ssim)) / (y_scale + 1 + 1)
                else:
                    y_ssim = "err_ssim_y"
                    u_ssim = "err_ssim_u"
                    v_ssim = "err_ssim_v"
                    yuv_ssim = "err_ssim_yuv"

                #  psnr data
                if psnr_line != "":
                    psnr_pieces = psnr_line.split()
                    y_psnr = psnr_pieces[4][2:]
                    u_psnr = psnr_pieces[5][2:]
                    v_psnr = psnr_pieces[6][2:]
                    yuv_psnr = (y_scale * float(y_psnr) + float(u_psnr) + float(v_psnr)) / (y_scale + 1 + 1)
                else:
                    y_psnr = "err_psnr_y"
                    u_psnr = "err_psnr_u"
                    v_psnr = "err_psnr_v"
                    yuv_psnr = "err_psnr_yuv"

                #  vmaf data
                if vmaf_line != "":
                    vmaf_pieces = vmaf_line.split("mean=\"")
                    y_vmaf = vmaf_pieces[1].split()[0][:-1]
                else:
                    y_vmaf = "err_vmaf"
                # if vmaf_line != '':
                    # y_vmaf = vmaf_line.split('VMAF score:')[-1].strip()
                # if vmaf_line != "":# and not vmaf_rc_flag:
                    # vmaf_pieces = vmaf_line.split("aggregateVMAF=\"")
                    # y_vmaf = vmaf_pieces[1].split()[0][:-1]

                # else:
                    # y_vmaf = "err_vmaf"

            else:
                #  ssim data
                if ssim_line != "":
                    print('ssim_line',ssim_line)
                    ssim_pieces = ssim_line.split("mean=\"")
                    y_ssim = ssim_pieces[1].split()[0][:-1]
                    u_ssim = "na_ssim_u"
                    v_ssim = "na_ssim_v"
                    yuv_ssim = "na_ssim_yuv"
                else:
                    y_ssim = "err_ssim_y"
                    u_ssim = "err_ssim_u"
                    v_ssim = "err_ssim_v"
                    yuv_ssim = "err_ssim_yuv"

                #  psnr data
                if psnr_line != "":
                    psnr_pieces = psnr_line.split("mean=\"")
                    y_psnr = psnr_pieces[1].split()[0][:-1]
                    # print(y_psnr)
                    # yuv_psnr = (y_scale * float(y_psnr) + float(u_psnr) + float(v_psnr)) / (y_scale + 1 + 1)
                    u_psnr = "na_psnr_u"
                    v_psnr = "na_psnr_v"
                    yuv_psnr = "na_psnr_yuv"
                else:
                    y_psnr = "err_psnr_y"
                    u_psnr = "err_psnr_u"
                    v_psnr = "err_psnr_v"
                    yuv_psnr = "err_psnr_yuv"

                #  vmaf data
                if vmaf_line != "":
                    vmaf_pieces = vmaf_line.split("mean=\"")
                    y_vmaf = vmaf_pieces[1].split()[0][:-1]
                else:
                    y_vmaf = "err_vmaf"

                if vmaf_neg_line != "":
                    vmaf_neg_pieces = vmaf_neg_line.split("mean=\"")
                    y_vmaf_neg = vmaf_neg_pieces[1].split()[0][:-1]
                else:
                    y_vmaf_neg = "err_vmaf_neg"

            #  bitrate data
            if bitrate_line != "":
                bitrate_pieces = bitrate_line.split()
##                print(bitrate_line)
##                print('bitrate_line.split(' ')[-6]',bitrate_line.split(' ')[-6])
                if "svt" in encoder:
                    bitrate = bitrate_pieces[4]
                elif "aom" in encoder:
                    if "bps" in bitrate_line:
                    #    print (bitrate_pieces[-4])
                        bitrate = float (bitrate_pieces[-4]) / 1000
                    else:
                        bitrate = float (bitrate_pieces[-5][:-3]) / 1000
                elif "vvenc" in encoder:
                    bitrate = bitrate_pieces[2]

                elif "x264" in encoder:
                    bitrate = bitrate_pieces[5]
                elif "x265" in encoder:
                    bitrate = bitrate_pieces[7]
                elif 'vp9' in encoder:
                    #print(bitrate_line.split(' ')[-6])
                    bitrate_line=[x for x in bitrate_line.split(' ') if x != '']

                    bitrate=float(bitrate_line[-5][:-3])/1000
                elif 'ffmpeg' in encoder:
                    #print('bitrate_line',bitrate_line)
                    if re.search(r'bitrate\=\s*\d+\.\d+', bitrate_line):
                        bitrate_line = re.findall(r'bitrate\=\s*\d+\.\d+', bitrate_line)[-1]
                        bitrate = float(bitrate_line.split('=')[-1].strip())

                    elif re.search(r'kb/s:\d+\.\d+', bitrate_line):
                        bitrate_line = re.findall(r'kb/s:\d+\.\d+', bitrate_line)[-1]
                        bitrate = bitrate_line.split(':')[-1].strip()

                    elif re.search(r'\s\d+\.\d+\skb/s', bitrate_line):
                        bitrate_line = re.findall(r'\s\d+\.\d+\skb/s', bitrate_line)[-1]
                        bitrate = bitrate_line.split('kb/s')[0].strip()

            else:
                bitrate = "err_bitrate"
##            print('online_bitrate',bitrate)

            try:
                if memory_line != "":
                    max_memory = "err_memory"
                    memory_pieces = memory_line.split()
                    mem_cap = memory_pieces[5]
                    max_memory = mem_cap
                else:
                    max_memory = "err_memory"
            except:
                max_memory = "err_memory"

            #  resolution data
            if res_line != "":
                res = re.findall(r'(\d\d\d?\d?x\d\d\d?\d?)',res_line)[0]
                res_pieces = res_line.split(',')
                # print('res_line',res_line)
                # resolution_pattern  =  r'\d+x\d+'#r"(\d\d\d?\d?x\d\d\d?\d?)"
                # last_match = re.findall(resolution_pattern,file_)[-1]
                # res = last_match
                # print('res',res)
                # res = res_pieces[5]
                # res = res.split(",")[0]
                if re.search(r'\(\d+b\)', res_line):
                    bd = re.findall(r'\(\d+b\)',res_line)[0]
                else:
                    bd = res_pieces[4][:-1]

                if "10le" in bd or '10b' in bd:
                    bd = "10bit"
                else:
                    bd = "8bit"
                if compute_convex_hull:
                   # print(file_)
                    resolution_pattern  =  r'\d+x\d+'#r"(\d\d\d?\d?x\d\d\d?\d?)"
                    last_match = re.findall(resolution_pattern,file_)[-1]
                    res = last_match

            else:
                res = "err_res"
                bd = "err_bit-depth"
            if fps_line !='' and frames_per_second!='':
                fps_piece = [x for x in fps_line.split(',') if 'fps' in x]
                frames_per_second = fps_piece[0].replace('fps','').strip()
            if offline_bitrate_calculation or filesize_instead:
                crf_encoded_file_pattern = r'\w+_M\d+_\S+?_Q\d+'
                vbr_encoded_file_pattern = r'\w+_M\d+_\S+?_TBR\d+'
                cwd=os.getcwd()
                res_split = re.search( r"to\d+x\d+", re.split(r"\d+x\d+", file_,maxsplit=1)[1])
                if res_split is None:
                    if len(re.findall(crf_encoded_file_pattern, file_)) != 0:
                        y4m_source_clip = os.path.join('resized_clips',re.split(r"_M\d+_", re.split(r"_Q\d\d", file_.replace("1920x1080_1920x1080_", "1920x1080"))[0])[1]+".y4m")
                        yuv_source_clip = os.path.join('resized_clips',re.split(r"_M\d+_", re.split(r"_Q\d\d", file_.replace("1920x1080_1920x1080_", "1920x1080"))[0])[1]+".yuv")
                    else:
                        y4m_source_clip = os.path.join('resized_clips',re.split(r"_M\d+_", re.split(r"_TBR\d+", file_.replace("1920x1080_1920x1080_", "1920x1080"))[0])[1]+".y4m")
                        yuv_source_clip = os.path.join('resized_clips',re.split(r"_M\d+_", re.split(r"_TBR\d+", file_.replace("1920x1080_1920x1080_", "1920x1080"))[0])[1]+".yuv")


                    bin_location=os.path.join(cwd,".".join(file_.split('.')[:-1])+'.bin')
                    bin_size=os.path.getsize(bin_location)
                    if os.path.isfile(y4m_source_clip):
                        source_clip = y4m_source_clip
                        _,_,fps_for_offline_calculation, frames_for_offline_calculation=read_y4m_header(source_clip)

                    elif os.path.isfile(yuv_source_clip):
                        source_clip = yuv_source_clip
                        og_width = float(res.split('x')[0])
                        og_height = float(res.split('x')[1])
##                        print('\n')
##                        print(yuv_source_clip)

                        if bd == '8bit':
                            frames_for_offline_calculation = int(os.path.getsize(source_clip)/(og_width*og_height+(og_width*og_height/2)))
                        elif bd == '10bit':
                            frames_for_offline_calculation = int(((os.path.getsize(source_clip)/(og_width*og_height+(og_width*og_height/2)))/2))

                        fps_for_offline_calculation = float(frames_per_second)
                    else:
                        print("Could not find file to parse fps and frame data from: ", y4m_source_clip)
                        quit()

                    if round(float(fps_for_offline_calculation))-float(fps_for_offline_calculation) == 0:
                        fps_denom=float(1000)
                    else:
                        fps_denom=float(1001)
                    fps_num=round(float(fps_for_offline_calculation))*1000
                    fps_for_offline_calculation = float(fps_num)/float(fps_denom)
                    #print(frames_for_offline_calculation)
                    #print(fps_for_offline_calculation)
                    #print('\n')

                    if encoder not in ['x264', 'x265']:
                        bin_size=bin_size-(32+(12*frames_for_offline_calculation))

                    if offline_bitrate_calculation:
                        bitrate = ((bin_size * 8)/frames_for_offline_calculation)*((fps_for_offline_calculation)/1000)


                else:

                    post_resolution = re.split(r"\d+x+\d+_lanc", re.split(r"\d+x\d+", file_,maxsplit=1)[1])[1]
                    pre_resolution = re.split(r"to\d+x+\d+_lanc_" , file_.replace("1920x1080_1920x1080", "1920x1080"))[0]
                    try:    ref_clip = re.split(r"_Q\d+", re.split(r"_M\d+_", re.findall(crf_encoded_file_pattern,file_)[0])[1])[0]
                    except: ref_clip = re.split(r"_TBR\d+", re.split(r"_M\d+_", re.findall(vbr_encoded_file_pattern,file_)[0])[1])[0]
                    y4m_source_clip = os.path.join('resized_clips',ref_clip+".y4m")
                    yuv_source_clip = os.path.join('resized_clips',ref_clip+".yuv")
                    #print(file_)
                    #print(yuv_source_clip)
                    bin_location=os.path.join(cwd,".".join(file_.split('.')[:-1])+'.bin')
                    bin_size=os.path.getsize(bin_location)

                    if os.path.isfile(y4m_source_clip):
                        source_clip = y4m_source_clip
                        _,_,fps_for_offline_calculation, frames_for_offline_calculation=read_y4m_header(source_clip)
                    elif os.path.isfile(yuv_source_clip):
                        source_clip = yuv_source_clip
                        og_width = float(res.split('x')[0])
                        og_height = float(res.split('x')[1])
                        print('\n')

                        if bd == '8bit':
                            frames_for_offline_calculation = int(os.path.getsize(source_clip)/(og_width*og_height+(og_width*og_height/2)))
                        elif bd == '10bit':
                            frames_for_offline_calculation = int(((os.path.getsize(source_clip)/(og_width*og_height+(og_width*og_height/2)))/2))
                        fps_for_offline_calculation = float(frames_per_second)

                    #print(frames_for_offline_calculation)
                    #print(fps_for_offline_calculation)
                    if round(float(fps_for_offline_calculation))-float(fps_for_offline_calculation) == 0:
                        fps_denom=float(1000)
                    else:
                        fps_denom=float(1001)
                    fps_num=round(float(fps_for_offline_calculation))*1000
                    fps_for_offline_calculation = float(fps_num)/float(fps_denom)
                    #print('\n')


                    if encoder not in ['x264', 'x265']:
                        bin_size=bin_size-(32+(12*frames_for_offline_calculation))

                    if offline_bitrate_calculation:
                        bitrate = ((bin_size * 8)/frames_for_offline_calculation)*(fps_for_offline_calculation/1000)
            # Set check
            set_name = ""
            for i in T1_SET:
                if i in file_name:
                    set_name = 'T1_TestSet'
                    break

            for i in T2_SET:
                if i in file_name:
                    if set_name != "":
                        set_name += '/T2_TestSet'
                    else:
                        set_name = 'T2_TestSet'
                    break

            for i in AOM_testset:
                if i in file_name:
                    if 'DOTA2_60f_420_720p' not in file_name:
                        if set_name != "":
                            set_name += '/AOM_TestSet'
                        else:
                            set_name = 'AOM_TestSet'
                        break

            for i in jctvc_60frames_TestSet:
                if i in file_name:
                    if set_name != "":
                        set_name += '/jctvc_60frames_TestSet'
                    else:
                        set_name = 'jctvc_60frames_TestSet'
                    break

            for i in jctv_fullframes_Testset:
                if i in file_name:
                    if '_60f' not in file_name:
                        if set_name != "":
                            set_name += '/jctv_fullframes_TestSet'
                        else:
                            set_name = 'jctv_fullframes_TestSet'
                        break

            for i in Testset_4k_14:
                if i in file_name:
                    if set_name != "":
                        set_name += '/4K_14_test_set'
                    else:
                        set_name = '4K_14_test_set'
                    break

            for i in TestSet_49f:
                if i in file_name:
                    if set_name != "":
                        set_name += '/49f_TestSet'
                    else:
                        set_name = '49f_TestSet'
                    break

            if set_name == "":
                bd += ', ' + 'undefined_list'
            else:
                bd += ', ' + set_name

            ## SC or Non-SC check
            if 'wikipedia' in file_name \
                    or 'MINECRAFT' in file_name \
                    or 'SlideEditing' in file_name \
                    or 'SlideShow' in file_name:
                bd += ', ' + 'SC'
            else:
                bd += ', ' + 'Non-SC'

        #  encode time data
            if enc_time_line != "":
                #print('enc_time_line',enc_time_line)
                enc_time_line = enc_time_line.replace('user', ' ')
                enc_time_pieces = enc_time_line.split()
                enc_time = enc_time_pieces[0]
                if "time" not in enc_time_line:
                    mins = float(enc_time_pieces[0][:enc_time_pieces[0].rfind('m')])
                    secs = float(enc_time_pieces[0][enc_time_pieces[0].rfind('m')+1:-1])

                    enc_time = str(((mins*60+secs)))
                    enc_time = str(int(float(enc_time)*1000))
                else:
                    if Num_passes == 2 and "svt" in encoder:
                        svt_1st_pass_time_line = svt_1st_pass_time_line.replace('user', ' ')
                        enc_time_pieces_1st = svt_1st_pass_time_line.split()
                        enc_time = float(float(enc_time_pieces[2] )+ float(enc_time_pieces_1st[2]))
                        enc_time = str(int(float(enc_time)*1000))
                    else:
                        enc_time = enc_time_pieces[2]
                        enc_time = str(int(float(enc_time)*1000))
            else:
                enc_time = "err_enc_time"

            #  system time data
            if sys_time_line != "":
                sys_time_line = sys_time_line.replace('system', ' ')
                sys_time_pieces = sys_time_line.split()
                sys_time = sys_time_pieces[0]
                if "time" not in sys_time_line:
                    #print(sys_time_line)
                    mins = float(sys_time_pieces[0][:sys_time_pieces[0].rfind('m')])
                    secs = float(sys_time_pieces[0][sys_time_pieces[0].rfind('m')+1:-1])

                    sys_time = str(((mins*60+secs)))
                    sys_time = str(int(float(sys_time)*1000))
                else:
                    if Num_passes == 2 and "svt" in encoder:
                        svt_1st_pass_time_line = svt_1st_pass_time_line.replace('system', ' ')
                        sys_time_pieces_1st = svt_1st_pass_time_line.split()
                        sys_time = float(float(sys_time_pieces[2] )+ float(sys_time_pieces_1st[2]))
                        sys_time = str(int(float(sys_time)*1000))
                    else:
                        sys_time = sys_time_pieces[2]
                        sys_time = str(int(float(sys_time)*1000))
            else:
                sys_time = "err_sys_time"

        #  qp data
            if "Q" in file_name:
                qp = file_name[-2:]
            elif "TBR" in file_name:
                qp = re.split('_TBR', file_name)[1]

            else:
                qp = "err_qp"

        #  get encoder_name
            try:
                delim_start_pos = re.search(r"_M-?[0-9]+",file_name).start()
                delim_end_pos = re.search(r"_M-?[0-9]+",file_name).end()
            except:
                delim_start_pos = 0
                delim_end_pos = 0
                pass

            delimiter = file_name[delim_start_pos:delim_end_pos]

            seq_name = file_name[delim_end_pos+1:-4]
            if compute_convex_hull == 1:
                res_id=re.findall(r"\d+x\d+to\d+x\d+",seq_name)#new method
                #print('res_id',res_id)
                if res_id==[]:
                    res_id=re.findall(r"\d+x\d+",seq_name)
                    if shift_resolution==1:
                        seq_name=seq_name.replace('_lanc','').replace('_'+res_id[0],'')
                        seq_name+='_'+res_id[0]
                else:
                    seq_name=seq_name.replace(res_id[0], res_id[0].split('to')[1]) #new method
                    if shift_resolution==1:
                        seq_name=seq_name.replace('_lanc','').replace('_'+str(res_id[0]).split('to')[1],'')
                        seq_name+='_'+res_id[0].split('to')[1]



            encoder_name = file_name[:delim_end_pos]

            wall_time_line = ""
            wall_time = ""

            enc_mode = delimiter.split('M')[1]
            time_enc_log_file_name = "time_enc_" + str(enc_mode) + ".log"
            if os.path.exists(time_enc_log_file_name):
                if int(enc_mode) < int(encoder_mode) and encoder != 'x264' and encoder != 'x265':
                    encoder_mode = str(enc_mode)
                elif int(enc_mode) > int(encoder_mode) and (encoder == 'x264' or encoder == 'x265'):
                    encoder_mode = str(enc_mode)
            else:
                enc_mode = str(encoder_mode)
                time_enc_log_file_name = "time_enc_" + str(enc_mode) + ".log"

            try:
                total_time_log = open(time_enc_log_file_name, 'r')

                log_lines = total_time_log.read().splitlines()
                for line in log_lines:
                    ## Check if using bash time instead of gnu time
                    if 'elapsed' in line.lower():
                        wall_time_line = line
                    elif 'real' in line.lower():
                        wall_time_line = line
                # print('\nwalltime : {}\n'.format(wall_time_line))
                if wall_time_line != "":
                    if "real" in wall_time_line :
                        wall_time_line = wall_time_line.replace('real', ' ')
                        wall_time_pieces = wall_time_line.split()
                        wall_time = wall_time_pieces[0]
                        if "CPU" not in wall_time_line:
                            mins = float(wall_time_pieces[0][:wall_time_pieces[0].rfind('m')])
                            secs = float(wall_time_pieces[0][wall_time_pieces[0].rfind('m')+1:-1])

                            wall_time = str(((mins*60+secs)))
                            wall_time = str(int(float(wall_time)*1000))
                    elif "Elapsed" in wall_time_line :
                        wall_time_pieces = wall_time_line.split()
                        wall_time = wall_time_pieces[7]

                        # NOTE: Will break if gnu time starts to use days past a certain threshold b/c Hours only pops up past 2 hours
                        mins = wall_time[:wall_time.rfind(':')]
                        hours = 0
                        if ":" in mins:
                            hours =  float(mins[:mins.rfind(':')])
                            mins = float(mins[(mins.rfind(':')+1):])
                        else:
                            mins = float(mins)
                        secs = float(wall_time[(wall_time.rfind(':')+1):])
                        wall_time = float(((hours*3600+mins*60+secs)))
                        wall_time = str(int(float(wall_time)*1000))

                else:
                        wall_time = "err_wall_time"
            except:
                wall_time = "err_wall_time"

            # print(wall_time)

            if get_enc_name_from_folder:
                path = os.path.abspath("..")
                parent_folder = os.path.basename(path)
                encoder_name = parent_folder+file_name[delim_start_pos:delim_end_pos]

            if force_enc_name:
                encoder_name = enc_name + file_name[delim_start_pos:delim_start_pos] + delimiter

            try:
                with open(str(file_).replace('.log','.xml')) as vmaf_extractor_result:
                    for line in vmaf_extractor_result:
                        if '<metric name="psnr_y"' in line:
                            psnr_y=line.split('mean="')[1].split('"')[0]
                            #print(psnr_y)
                        if '<metric name="float_ssim"' in line:
                            float_ssim=line.split('mean="')[1].split('"')[0]

                        if '<metric name="vmaf"' in line:
                            vmaf=line.split('mean="')[1].split('"')[0]
                        if '<metric name="vmaf_neg"' in line:
                            vmaf_neg=line.split('mean="')[1].split('"')[0]
            except:
                pass

            if metrics_computation == 1:
                output =   (   str(encoder)
                            +"\t" + str(encoder_name)
                            +"\t" + str(res)
                            +"\t" + str(bd)
                            +"\t" + str(seq_name)
                            +"\t" + str(qp)
                            +"\t" + str(bitrate)
                            +"\t" + str(y_psnr)
                            +"\t" + str(u_psnr)
                            +"\t" + str(v_psnr)
                            +"\t" + str(yuv_psnr)
                            +"\t" + str(y_ssim)
                            +"\t" + str(u_ssim)
                            +"\t" + str(v_ssim)
                            +"\t" + str(yuv_ssim)
                            +"\t" + str(y_vmaf)
                            +"\t" + str(enc_time)
                            +"\t" + str(wall_time)
                            +"\t" + str(max_memory)
                            +"\t" + str(sys_time)
                               )
                if filesize_instead == 1:
                    output =   (   str(encoder)
                                +"\t" + str(encoder_name)
                                +"\t" + str(res)
                                +"\t" + str(bd)
                                +"\t" + str(seq_name)
                                +"\t" + str(qp)
                                +"\t" + str(bin_size)
                                +"\t" + str(y_psnr)
                                +"\t" + str(u_psnr)
                                +"\t" + str(v_psnr)
                                +"\t" + str(yuv_psnr)
                                +"\t" + str(y_ssim)
                                +"\t" + str(u_ssim)
                                +"\t" + str(v_ssim)
                                +"\t" + str(yuv_ssim)
                                +"\t" + str(y_vmaf)
                                +"\t" + str(enc_time)
                                +"\t" + str(wall_time)
                                +"\t" + str(max_memory)
                                +"\t" + str(sys_time)
                                   )
                if keep_both_bitrate_filesize_column and filesize_instead:
                    output =   (   str(encoder)
                                +"\t" + str(encoder_name)
                                +"\t" + str(res)
                                +"\t" + str(bd)
                                +"\t" + str(seq_name)
                                +"\t" + str(qp)
                                +"\t" + str(bin_size)
                                +"\t" + str(y_psnr)
                                +"\t" + str(u_psnr)
                                +"\t" + str(v_psnr)
                                +"\t" + str(yuv_psnr)
                                +"\t" + str(y_ssim)
                                +"\t" + str(u_ssim)
                                +"\t" + str(v_ssim)
                                +"\t" + str(yuv_ssim)
                                +"\t" + str(y_vmaf)
                                +"\t" + str(enc_time)
                                +"\t" + str(wall_time)
                                +"\t" + str(max_memory)
                                +"\t" + str(sys_time)
                                +"\t" + str(bitrate)
                                   )


            elif metrics_computation == 2: #vmaf
                output =   (   str(encoder)
                            +"\t" + str(encoder_name)
                            +"\t" + str(res)
                            +"\t" + str(bd)
                            +"\t" + str(seq_name)
                            +"\t" + str(qp)
                            +"\t" + str(bitrate)
                            +"\t" + str(psnr_y)
                            +"\t" + str(float_ssim)
                            +"\t" + str(vmaf)
                            +"\t" + str(vmaf_neg)
                            +"\t" + str(enc_time)
                            +"\t" + str(wall_time)
                            +"\t" + str(max_memory)
                            +"\t" + str(sys_time))
                if filesize_instead == 1:
                    output =   (   str(encoder)
                                +"\t" + str(encoder_name)
                                +"\t" + str(res)
                                +"\t" + str(bd)
                                +"\t" + str(seq_name)
                                +"\t" + str(qp)
                                +"\t" + str(bin_size)
                                +"\t" + str(psnr_y)
                                +"\t" + str(float_ssim)
                                +"\t" + str(vmaf)
                                +"\t" + str(vmaf_neg)
                                +"\t" + str(enc_time)
                                +"\t" + str(wall_time)
                                +"\t" + str(max_memory)
                                +"\t" + str(sys_time))
                if keep_both_bitrate_filesize_column and filesize_instead:
                    output =   (   str(encoder)
                                +"\t" + str(encoder_name)
                                +"\t" + str(res)
                                +"\t" + str(bd)
                                +"\t" + str(seq_name)
                                +"\t" + str(qp)
                                +"\t" + str(bin_size)
                                +"\t" + str(psnr_y)
                                +"\t" + str(float_ssim)
                                +"\t" + str(vmaf)
                                +"\t" + str(vmaf_neg)
                                +"\t" + str(enc_time)
                                +"\t" + str(wall_time)
                                +"\t" + str(max_memory)
                                +"\t" + str(sys_time)
                                +"\t" + str(bitrate)
                                   )

            else:
                output =   (   str(encoder)
                            +"\t" + str(encoder_name)
                            +"\t" + str(res)
                            +"\t" + str(bd)
                            +"\t" + str(seq_name)
                            +"\t" + str(qp)
                            +"\t" + str(bitrate)
                            +"\t" + str(y_psnr)
                            +"\t" + str(u_psnr)
                            +"\t" + str(v_psnr)
                            +"\t" + str(yuv_psnr)
                            +"\t" + str(y_ssim)
                            +"\t" + str(u_ssim)
                            +"\t" + str(v_ssim)
                            +"\t" + str(yuv_ssim)
                            +"\t" + str(y_vmaf)
                            +"\t" + str(y_vmaf_neg)
                            +"\t" + str(enc_time)
                            +"\t" + str(wall_time)
                            +"\t" + str(max_memory)
                            +"\t" + str(sys_time)
                               )
                if filesize_instead == 1:
                    output =   (   str(encoder)
                                +"\t" + str(encoder_name)
                                +"\t" + str(res)
                                +"\t" + str(bd)
                                +"\t" + str(seq_name)
                                +"\t" + str(qp)
                                +"\t" + str(bin_size)
                                +"\t" + str(y_psnr)
                                +"\t" + str(u_psnr)
                                +"\t" + str(v_psnr)
                                +"\t" + str(yuv_psnr)
                                +"\t" + str(y_ssim)
                                +"\t" + str(u_ssim)
                                +"\t" + str(v_ssim)
                                +"\t" + str(yuv_ssim)
                                +"\t" + str(y_vmaf)
                                +"\t" + str(y_vmaf_neg)
                                +"\t" + str(enc_time)
                                +"\t" + str(wall_time)
                                +"\t" + str(max_memory)
                                +"\t" + str(sys_time))
                if keep_both_bitrate_filesize_column and filesize_instead:
                    output =   (   str(encoder)
                                +"\t" + str(encoder_name)
                                +"\t" + str(res)
                                +"\t" + str(bd)
                                +"\t" + str(seq_name)
                                +"\t" + str(qp)
                                +"\t" + str(bin_size)
                                +"\t" + str(y_psnr)
                                +"\t" + str(u_psnr)
                                +"\t" + str(v_psnr)
                                +"\t" + str(yuv_psnr)
                                +"\t" + str(y_ssim)
                                +"\t" + str(u_ssim)
                                +"\t" + str(v_ssim)
                                +"\t" + str(yuv_ssim)
                                +"\t" + str(y_vmaf)
                                +"\t" + str(y_vmaf_neg)
                                +"\t" + str(enc_time)
                                +"\t" + str(wall_time)
                                +"\t" + str(max_memory)
                                +"\t" + str(sys_time)
                                +"\t" + str(bitrate)
                                   )

            full_data_for_writing.append(output)
            #print_log (output,'a', index_i);
#Sort the data
#sorted_data_for_writing=sorted([x.split('\t') for x in full_data_for_writing], key=lambda x: (x[1],x[4]))
sorted_data_for_writing=sorted([x.split('\t') for x in full_data_for_writing], key=lambda x: (x[1],x[4]))
for write_line in sorted_data_for_writing:
    out_log.write("\t".join(write_line))
    out_log.write('\n')
out_log.close()


if compute_convex_hull == 1:
    for cvh_metric in cvh_metrics:
        f_in        = open(log_name,'r');
        lines       = f_in.readlines();
        all_lines   = [[]]
        clip_lines  = []
        active_clip = ""
        temp_clip   = ""
        header = ""
        output_file = cvh_metric + output_file_suffix.split('.')[0]+'_' + enc_name+'.txt'

        for l in lines:
            if "EncoderName" in l:
                header = l
                continue

            if active_clip != "":
                line_clip_name = ""
                l_clip = ""
                l_pieces = l.split("\t")
                line_clip_name = l_pieces[4]
                l_match = re.search("_\d+x\d+", line_clip_name)
                #print('l_match',l_match)
                if l_match is not None:
                    l_clip = line_clip_name[:l_match.start()]
                    #print('l_clip',l_clip)
                else:
                    l_cn_pieces = line_clip_name.split("_")
                    if len(l_cn_pieces) < 2:
                        l_clip = line_clip_name
                    else:
                        l_clip = l_cn_pieces[0]+"_"+l_cn_pieces[1]
                    print('no match l_clip',l_clip)

                if active_clip == l_clip and encoder_name in l:
                    clip_lines.append(l)
                else:
                    # new clip
                    #print (clip_lines)
                    all_lines.append(clip_lines)
                    clip_lines = []

            if clip_lines == []:

                # reset
                active_clip = ""
                clip_name = ""
                encoder_name = ""

                # new clip
                clip_lines.append(l)
                #print('clip_lines',clip_lines)
                pieces = l.split("\t")
##                print(len(pieces))
                # check length of line
                if metrics_computation == 1:
                    if len(pieces) != 20  and len(pieces) !=21:
                        print ("File format does not seem to be correct")
                        continue
                elif metrics_computation == 2:
                    if len(pieces) != 15 and len(pieces) !=16:
                        print ("File format does not seem to be correct")
                        continue
                else:
                    if len(pieces) != 21  and len(pieces) !=22:
                        print ("File format does not seem to be correct")
                        continue

                # extract clip name
                clip_name = pieces[4]
                encoder_name = pieces[1]

                # extract root clip name
                match = re.search("_\d+x\d+", clip_name)
                if match is not None:
                    active_clip = clip_name[:match.start()]
                else:
                    nm_p = clip_name.split("_")
                    if len(nm_p) < 2:
                        active_clip = clip_name

                    active_clip = nm_p[0]+"_"+nm_p[1]


        # add last clip

        all_lines.append(clip_lines)

        if header == "":
            if metrics_computation == 1:
                header = "Codec\tEncoderName\tResolution\tBit-Depth\tInputSequence\tQP\tKbps\tPSNR(Y)\tPSNR(U)\tPSNR(V)\tPSNR(ALL)\tSSIM(Y)\tSSIM(U)\tSSIM(V)\tSSIM(ALL)\tVMAF\tenc_time(ms)\twall_time(ms)\tmax_memory(kb)"
            elif metrics_computation == 2:
                header = "Codec\tEncoderName\tResolution\tBit-Depth\tInputSequence\tQP\tKbps\tPSNR(Y)\tFloat_SSIM\tVMAF\tVMAF NEG\tenc_time(ms)\twall_time(ms)\tmax_memory(kb)\tsys_time(ms)"
            else:
                header = "Codec\tEncoderName\tResolution\tBit-Depth\tInputSequence\tQP\tKbps\tPSNR(Y)\tPSNR(U)\tPSNR(V)\tPSNR(ALL)\tSSIM(Y)\tSSIM(U)\tSSIM(V)\tSSIM(ALL)\tVMAF\tVMAF_neg\tenc_time(ms)\twall_time(ms)\tmax_memory(kb)"

        print (header,file=open( output_file,'w'))
        for i in range(len(all_lines)):
            # print('\n')
            # print('all_lines',all_lines[i])
            # print('\n')

            # time.sleep(3)
            conv_h_exec  = "./tools/convex_hull_exe "
            rates       = " "
            metric      = " "
            new_lines   = []
            if all_lines[i] == []:
                continue
            for line in all_lines[i]:

#cvh_metrics = ["psnr","float_ssim", "float_ms-ssim",'vmaf','vmaf_neg']

                line_s = line.split("\t")
                if metrics_computation == 1:
                    if len(line_s) != 20 and len(line_s) != 21: #accounts for the potential extra column
                        print ("File format does not seem to be correct")
                        continue
                elif metrics_computation == 2:
                    if len(line_s) != 15 and len(line_s) !=16:
                        print ("File format does not seem to be correct")
                        continue
                else:
                    if len(line_s) != 21 and len(line_s) != 22:
                        print ("File format does not seem to be correct")
                        continue

                rates   += (line_s[6] + "  ")
                if metrics_computation == 2:
                    if cvh_metric == "psnr":
                        metric += (line_s[7] + "  ")
                    elif cvh_metric == "vmaf":
                        metric += (line_s[9] + "  ")
                    elif cvh_metric == "vmaf-neg":
                        metric += (line_s[10] + "  ")
                    else:#ssim
                        metric += (line_s[8] + "  ")
                else:
                    if cvh_metric == "psnr":
                        metric += (line_s[7] + "  ")
                    elif cvh_metric == "vmaf":
                        metric += (line_s[15] + "  ")
                    else:
                        metric += (line_s[11] + "  ")

            #print('all_lines[i]',all_lines[i])
            conv_h_cli = conv_h_exec + str(len(all_lines[i])) + rates + metric + " > conv_h_log.txt"
##            print('conv_h_cli',conv_h_cli)
            #print('conv_h_cli',conv_h_cli)
            # run the convex hull commandline
            os.system(conv_h_cli)
            # get the convex hull indices
            index_range = list(range(len(all_lines[i])))
            #print('index_range',index_range)
            with open('conv_h_log.txt', 'r') as log:
                lines = log.readlines()

                for line in lines:
                    if "i=" in line:
                        line_s = line.split(",")
                        index_w = line_s[0]
                        index = int(index_w[2:])

                        # prep line to be added
                        tmp_line = all_lines[i][index]
                        index_range.remove(index)
                        clip_name = tmp_line.split("\t")[4]
                        match = re.search("_\d+x\d+", clip_name)
                        if match is not None:
                            sim_clip_name = clip_name[:match.start()]
                        else:
                            if len(clip_name.split("_")) < 2:
                                sim_clip_name = clip_name
                            sim_clip_name = clip_name.split("_")[0]+"_"+clip_name.split("_")[1]

                        # rename clip to unify the clip name as much as possible
                        rep_line = tmp_line.replace(clip_name,sim_clip_name)
                        rep_line_s = rep_line.split("\t")

                        if metrics_computation == 1:
                            metric_part = rep_line_s[7]+"\t"+" \t"*8
                            if cvh_metric == "vmaf":
                                metric_part = " \t"*8 + rep_line_s[15]+"\t"
                            elif cvh_metric == "ssim":
                                metric_part = " \t"*4 + rep_line_s[11]+"\t" + " \t"*4

                            fn_line     = (   rep_line_s[0]+"\t"
                                            + rep_line_s[1]+"_"+cvh_metric+"\t"
                                            + rep_line_s[2]+"\t"
                                            + rep_line_s[3]+"\t"
                                            + rep_line_s[4]+"\t"
                                            + rep_line_s[5]+"\t"
                                            + rep_line_s[6]+"\t"
                                            + metric_part
                                            + rep_line_s[16]+"\t"
                                            + rep_line_s[17]+"\t"
                                            + rep_line_s[18]+"\t"
                                            + rep_line_s[19])

                        elif metrics_computation == 2:
                            metric_part = rep_line_s[7]+"\t"+" \t"*4
                            if cvh_metric == "ssim":
                                metric_part = " \t"*1 + rep_line_s[8]+"\t" + " \t"*3
                            if cvh_metric == "vmaf":
                                metric_part = " \t"*3 + rep_line_s[9]+"\t"+ " \t"*1
                            elif cvh_metric == "vmaf-neg":
                                metric_part = " \t"*4 + rep_line_s[10]+"\t"
                            fn_line     = (   rep_line_s[0]+"\t"
                                            + rep_line_s[1]+"_"+cvh_metric+"\t"
                                            + rep_line_s[2]+"\t"
                                            + rep_line_s[3]+"\t"
                                            + rep_line_s[4]+"\t"
                                            + rep_line_s[5]+"\t"
                                            + rep_line_s[6]+"\t"
                                            + metric_part
                                            + rep_line_s[11]+"\t"
                                            + rep_line_s[12]+"\t"
                                            + rep_line_s[13]+"\t"
                                            + rep_line_s[14])
                        else:
                            metric_part = rep_line_s[7]+"\t"+" \t"*9
                            if cvh_metric == "vmaf":
                                metric_part = " \t"*8 + rep_line_s[15]+"\t"*2
                            elif cvh_metric == "ssim":
                                metric_part = " \t"*4 + rep_line_s[11]+"\t" + " \t"*5
                            elif cvh_metric == "vmaf_neg":
                                metric_part = " \t"*9 + rep_line_s[16]+"\t"

                            fn_line     = (   rep_line_s[0]+"\t"
                                            + rep_line_s[1]+"_"+cvh_metric+"\t"
                                            + rep_line_s[2]+"\t"
                                            + rep_line_s[3]+"\t"
                                            + rep_line_s[4]+"\t"
                                            + rep_line_s[5]+"\t"
                                            + rep_line_s[6]+"\t"
                                            + metric_part
                                            + rep_line_s[17]+"\t"
                                            + rep_line_s[18]+"\t"
                                            + rep_line_s[19]+"\t"
                                            + rep_line_s[20])

                        new_lines.append(fn_line)

            # output the results
            line_count = 0
            for line in reversed(new_lines):
                line_count += 1
                if line_count > max_ch_pts:
                    continue
                if line[-1] == "\n":
                    print (line[:-1],file=open( output_file,'a'))
                else:
                    print (line,file=open( output_file,'a'))


            line = new_lines[0]
            if line_count > max_ch_pts:
                print ("\n\n\n\n\n MAX line count reached or exceeded " + str(line_count)+ "\n\n\n\n")
            else:
                max_qp = 64
                for j in index_range:
                    if (line_count<max_ch_pts):
                        filler_pieces = all_lines[i][j].split("\t")
##                        print('filler_pieces',filler_pieces)
                        if metrics_computation == 2:
                            filler_line = (   filler_pieces[0]+"\t"
                                            + filler_pieces[1]+"_"+cvh_metric+"\t"
                                            + filler_pieces[2]+"\t"
                                            + filler_pieces[3]+"\t"
                                            + line.split("\t")[4]+"\t"
                                            + str(max_qp)
                                            + " \t"*7
                                            + filler_pieces[11]+"\t"
                                            + filler_pieces[12]+"\t"
                                            + filler_pieces[13]+"\t"
                                            + filler_pieces[14].rstrip())

                        else:
                            filler_line = (   filler_pieces[0]+"\t"
                                            + filler_pieces[1]+"_"+cvh_metric+"\t"
                                            + filler_pieces[2]+"\t"
                                            + filler_pieces[3]+"\t"
                                            + line.split("\t")[4]+"\t"
                                            + str(max_qp)
                                            + " \t"*11
                                            + filler_pieces[16]+"\t"
                                            + filler_pieces[17]+"\t"
                                            + filler_pieces[18]+"\t"
                                            + filler_pieces[19].rstrip())
                        #print('filler_line',filler_line) ### all good here
                        print (filler_line,file=open( output_file,'a'))
                        line_count = line_count + 1
                        max_qp = max_qp + 1
                    else:
                        break

                while(line_count<max_ch_pts):
                    #print('line',line)
                    filler_pieces = line.split("\t")
                    if metrics_computation == 1:
                        filler_line = (   filler_pieces[0]+"\t"
                                        + filler_pieces[1]+"_"+cvh_metric+"\t"
                                        + filler_pieces[2]+"\t"
                                        + filler_pieces[3]+"\t"
                                        + filler_pieces[4]+"\t"
                                        + str(max_qp))
                    else:
                        filler_line = (   filler_pieces[0]+"\t"
                                        + filler_pieces[1]+"_"+cvh_metric+"\t"
                                        + filler_pieces[2]+"\t"
                                        + filler_pieces[3]+"\t"
                                        + filler_pieces[4]+"\t"
                                        + str(max_qp))
                    #print('filler_line',filler_line)
                    print(filler_line,file=open(output_file,'a'))
                    line_count = line_count + 1
                    max_qp = max_qp + 1
