FROM @CI_REGISTRY_IMAGE@/ubuntu2004:latest

ARG DATE=1

RUN apt-get update && \
    apt-get install -yyy libarchive-tools && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ARG _freebsd_version=13.0 \
    _openbsd_version=7.1

WORKDIR /opt/cross-freebsd

RUN curl -Ls "https://download.freebsd.org/ftp/releases/amd64/${_freebsd_version}-RELEASE/base.txz" | bsdtar xf - lib usr/include usr/lib && \
    printf %s\\n \
    "set(FREEBSD_VERSION ${_freebsd_version})" \
    'set(CMAKE_SYSTEM_NAME FreeBSD)' \
    'set(CMAKE_SYSTEM_PROCESSOR amd64)' \
    '' \
    'set(CMAKE_SYSROOT /opt/cross-freebsd)' \
    '' \
    'set(CMAKE_C_COMPILER clang)' \
    'set(CMAKE_CXX_COMPILER clang++)' \
    'set(CMAKE_C_COMPILER_TARGET x86_64-unknown-freebsd${FREEBSD_VERSION})' \
    'set(CMAKE_CXX_COMPILER_TARGET x86_64-unknown-freebsd${FREEBSD_VERSION})' \
    '' \
    'set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)' \
    'set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)' \
    'set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)' \
    'set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)' \
    'option(BUILD_SHARED_LIBS "Build shared libraries (DLLs)." OFF)' \
    > toolchain.cmake

WORKDIR /opt/cross-openbsd

RUN curl -Ls "https://ftp.openbsd.org/pub/OpenBSD/${_openbsd_version}/amd64/base$(echo "$_openbsd_version" | sed s/\\.//).tgz" | bsdtar xf - usr/include usr/lib && \
    curl -Ls "https://ftp.openbsd.org/pub/OpenBSD/${_openbsd_version}/amd64/comp$(echo "$_openbsd_version" | sed s/\\.//).tgz" | bsdtar xf - usr/include usr/lib && \
    printf %s\\n \
    "set(OPENBSD_VERSION ${_openbsd_version})" \
    'set(CMAKE_SYSTEM_NAME OpenBSD)' \
    'set(CMAKE_SYSTEM_PROCESSOR amd64)' \
    '' \
    'set(CMAKE_SYSROOT /opt/cross-openbsd)' \
    '' \
    'set(CMAKE_C_COMPILER clang)' \
    'set(CMAKE_CXX_COMPILER clang++)' \
    'set(CMAKE_C_COMPILER_TARGET x86_64-unknown-openbsd${OPENBSD_VERSION})' \
    'set(CMAKE_CXX_COMPILER_TARGET x86_64-unknown-openbsd${OPENBSD_VERSION})' \
    '' \
    'set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)' \
    'set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)' \
    'set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)' \
    'set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)' \
    'option(BUILD_SHARED_LIBS "Build shared libraries (DLLs)." OFF)' \
    > toolchain.cmake

WORKDIR /
